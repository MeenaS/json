export interface IProduct {
    lamb: string,
    goat: string,
    chicken: string,
    egg: string
}