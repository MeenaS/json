import { Component, OnInit } from '@angular/core';
import {ProductService} from '../product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public products =[];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
    .subscribe(data => this.products = data);
  }

}
