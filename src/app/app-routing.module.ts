import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { LoginComponent } from './login/login.component';
import { GoatComponent } from './goat/goat.component';


const routes: Routes = [
 
  { path: 'goat', component: GoatComponent },
  { path: 'login', component: LoginComponent}
 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}